<!DOCTYPE HTML>
<html>
<head>
    <title> Grade_Sheet  </title>
    <style>
    .error {color: #FF0000;}
    </style>
</head>

<body
<?php
// define variables and set to empty values
$name = $id = $section = $department = $marks = NULL;
$ner = $ier = $ser = $der = $mer = NULL;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["n"])) {
        $ner= "Name is required";
    }
    else {
        $name = input($_POST["n"]);
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $ner = "Only letters and white space allowed";
        }
    }
    if (empty($_POST["i"])) {
        $ier= "Id is required";
    }else {
        $id = input($_POST["i"]);
        if (!preg_match("/^[0-9]*$/",$name)) {
            $ier = "Only numbers and white space allowed";
        }
    }
    if (empty($_POST["s"])) {
        $ser= "Section is required";
    }else {
        $section = input($_POST["s"]);
    }
    if (empty($_POST["d"])) {
        $der= "Department is required";
    }else {
        $department = input($_POST["d"]);
    }
    if (empty($_POST["m"])) {
        $mer= "Marks is required";
    }else {
        $marks = input($_POST["m"]);
    }

}

function input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<h2> || Student Grading System Input Data || </h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    Name: <input type="text" name="n">
    <span class="error">* <?php echo $ner;?></span>
    <br><br>
    ID: <input type="text" name="i">
    <span class="error">* <?php echo $ier;?></span>
    <br><br>
    Section: <input type="text" name="s">
    <span class="error">* <?php echo $ser;?></span>
    <br><br>
    Department: <input type="text" name="d">
    <br><br>
    Marks: <input type="text" name="m">
    <span class="error">* <?php echo $mer;?></span>
    <br><br>
    <input type="submit" name="submit" value="Submit">
</form>

<?php
echo "<h2> Student Data </h2>";
if(preg_match("/^[a-zA-Z ]*$/",$name)){
    echo $name;
}
echo "<br>";
if (preg_match("/^[0-9]*$/",$name)) {
    echo $id;
}
echo "<br>";
echo $section;
echo "<br>";
echo $department;
echo "<br>";
echo $marks;
echo "<br>";
if ($marks >= 80){
    echo "A+";
}
?>

</body>
</html>